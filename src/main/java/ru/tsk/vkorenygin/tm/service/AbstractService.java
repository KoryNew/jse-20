package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.api.service.IService;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.entity.EntityNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public boolean existsById(final String id) {
        if (DataUtil.isEmpty(id))
            return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            return false;
        return repository.existsByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        if (comparator == null)
            return null;
        return repository.findAll(comparator);
    }

    @Override
    public E findByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > repository.getSize())
            throw new EntityNotFoundException();
        return repository.findByIndex(index);
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (DataUtil.isEmpty(collection)) return;
        repository.addAll(collection);
    }

    @Override
    public E findById(final String id) throws EmptyIdException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E removeById(final String id) throws EmptyIdException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        final E entity = repository.findByIndex(index);
        if (entity == null)
            throw new EntityNotFoundException();
        return repository.removeByIndex(index);
    }

    @Override
    public E remove(E entity) {
        if (entity == null) return null;
        return repository.remove(entity);
    }

}
