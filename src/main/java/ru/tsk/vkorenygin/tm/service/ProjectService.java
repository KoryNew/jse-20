package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.util.DataUtil;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public void create(String name, String description, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public Project findByIndex(int index, String userId) throws AbstractException {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.findByIndex(index, userId);
    }

    @Override
    public Project findByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.findByName(name, userId);
    }

    @Override
    public Project changeStatusById(String id, Status status, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return projectRepository.changeStatusById(id, status, userId);
    }

    @Override
    public Project changeStatusByName(String name, Status status, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public Project startById(String id, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.startById(id, userId);
    }

    @Override
    public Project startByIndex(Integer index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.startByIndex(index, userId);
    }

    @Override
    public Project startByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.startByName(name, userId);
    }

    @Override
    public Project finishById(String id, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.finishById(id, userId);
    }

    @Override
    public Project finishByIndex(Integer index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index, userId);
    }

    @Override
    public Project finishByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.finishByName(name, userId);
    }

    @Override
    public Project updateByIndex(Integer index,
                                 String name,
                                 String description,
                                 String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Project project = projectRepository.findByIndex(index, userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(String id,
                              String name,
                              String description,
                              String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Project project = projectRepository.findById(id, userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByIndex(int index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        Project project = projectRepository.findByIndex(index, userId);
        if (project == null)
            throw new ProjectNotFoundException();
        return projectRepository.removeByIndex(index, userId);
    }

    @Override
    public Project removeByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Project project = projectRepository.findByName(name, userId);
        if (project == null)
            throw new ProjectNotFoundException();
        return projectRepository.removeByName(name, userId);
    }

}
