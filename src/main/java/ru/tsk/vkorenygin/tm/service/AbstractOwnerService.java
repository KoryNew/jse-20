package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.api.service.IOwnerService;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> implements IOwnerService<E> {

    protected IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        this.ownerRepository = repository;
    }

    @Override
    public E add(E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        ownerRepository.add(entity);
        return entity;
    }

    public boolean existsById(String id, String userId) {
        return ownerRepository.existsById(id, userId);
    }

    public boolean existsByIndex(Integer index, String userId) {
        return ownerRepository.existsByIndex(index, userId);
    }

    @Override
    public List<E> findAll(String userId) {
        return ownerRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(Comparator<E> comparator, String userId) {
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(comparator, userId);
    }

    @Override
    public E findById(String id, String userId) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.findById(id, userId);
    }

    @Override
    public E findByIndex(int index, String userId) throws AbstractException {
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.findByIndex(index, userId);
    }

    @Override
    public E removeById(String id, String userId) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.removeById(id, userId);
    }

    @Override
    public E removeByIndex(int index, String userId) throws AbstractException {
        if (index < 0) throw new IncorrectIndexException();
        return ownerRepository.removeByIndex(index, userId);
    }

    @Override
    public void remove(E entity, String userId) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        ownerRepository.remove(entity, userId);
    }

    @Override
    public void clear(String userId) {
        ownerRepository.clear(userId);
    }

}

