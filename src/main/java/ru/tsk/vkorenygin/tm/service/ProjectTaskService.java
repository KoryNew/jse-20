package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectTaskService;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(String projectId, String taskId, String userId) {
        if (DataUtil.isEmpty(projectId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId))
            throw new EmptyIdException();
        if (!projectRepository.existsById(projectId, userId))
            throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId, userId))
            throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(projectId, taskId, userId);
    }

    @Override
    public Task unbindTaskFromProject(String projectId, String taskId, String userId) {
        if (DataUtil.isEmpty(projectId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId))
            throw new EmptyIdException();
        if (!projectRepository.existsById(projectId, userId))
            throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId, userId))
            throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(taskId, userId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(String id, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (!projectRepository.existsById(id, userId))
            throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(id, userId);
    }

}
