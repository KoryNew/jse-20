package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;


public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public void create(String name, String description, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public Task findByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.findByName(name, userId);
    }

    @Override
    public Task changeStatusById(String id, Status status, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return taskRepository.changeStatusById(id, status, userId);
    }

    @Override
    public Task changeStatusByName(String name, Status status, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public Task startById(String id, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.startById(id, userId);
    }

    @Override
    public Task startByIndex(Integer index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.startByIndex(index, userId);
    }

    @Override
    public Task startByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.startByName(name, userId);
    }

    @Override
    public Task finishById(String id, String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.finishById(id, userId);
    }

    @Override
    public Task finishByIndex(Integer index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.finishByIndex(index, userId);
    }

    @Override
    public Task finishByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.finishByName(name, userId);
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Task task = taskRepository.findByIndex(index, userId);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(String id, String name, String description, String userId) {
        if (DataUtil.isEmpty(id))
            return null;
        if (DataUtil.isEmpty(name))
            return null;
        Task task = taskRepository.findById(id, userId);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByIndex(int index, String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.removeByIndex(index, userId);
    }

    @Override
    public Task removeByName(String name, String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.removeByName(name, userId);
    }

}
