package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    E add(E entity) throws AbstractException;

    boolean existsById(String id, String userId);

    boolean existsByIndex(Integer index, String userId);

    List<E> findAll(String userId);

    List<E> findAll(Comparator<E> comparator, String userId);

    E findById(String id, String userId) throws AbstractException;

    E findByIndex(int index, String userId) throws AbstractException;

    E removeById(String id, String userId) throws AbstractException;

    E removeByIndex(int index, String userId) throws AbstractException;

    void remove(E entity, String userId) throws AbstractException;

    void clear(String userId);

}

