package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void addAll(final Collection<E> collection);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    int getSize();

    List<E> findAll();

    List<E> findAll(final Comparator<E> comparator);

    E findById(String id) throws EmptyIdException;

    E findByIndex(Integer index);

    void clear();

    E removeById(String id) throws EmptyIdException;

    E removeByIndex(Integer index);

    E remove(E entity);

}
