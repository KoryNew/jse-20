package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    void create(String name, String userId);

    void create(String name, String description, String userId);

    Task findByName(String name, String userId);

    Task changeStatusById(final String id, final Status status, final String userId);

    Task changeStatusByName(final String name, final Status status, final String userId);

    Task changeStatusByIndex(final Integer index, final Status status, final String userId);

    Task startById(String id, String userId);

    Task startByIndex(Integer index, String userId);

    Task startByName(String name, String userId);

    Task finishById(String id, String userId);

    Task finishByIndex(Integer index, String userId);

    Task finishByName(String name, String userId);

    Task updateById(final String id,
                    final String name,
                    final String description,
                    final String userId);

    Task updateByIndex(final Integer index,
                       final String name,
                       final String description,
                       final String userId);

    Task removeByName(String name, String userId);

}
