package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyEmailException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyLoginException;
import ru.tsk.vkorenygin.tm.entity.User;

public interface IUserService extends IService<User> {

    boolean isLoginExists(String login) throws EmptyLoginException;

    boolean isEmailExists(String email) throws EmptyLoginException, EmptyEmailException;

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User findByLogin(String login) throws EmptyLoginException;

    User findByEmail(String email) throws EmptyEmailException;

    User removeByLogin(String login) throws EmptyLoginException;

    User setPassword(String userId, String password) throws AbstractException;

    User updateUser(String userId, String firstName, String lastName, String middleName) throws EmptyIdException;

}
