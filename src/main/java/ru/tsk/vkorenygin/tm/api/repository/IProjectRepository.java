package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    int getSize();

    Project findByName(String name, String userId);

    Project changeStatusById(String id, Status status, String userId);

    Project changeStatusByName(String name, Status status, String userId);

    Project changeStatusByIndex(Integer index, Status status, String userId);

    Project startById(String id, String userId);

    Project startByIndex(Integer index, String userId);

    Project startByName(String name, String userId);

    Project finishById(String id, String userId);

    Project finishByIndex(Integer index, String userId);

    Project finishByName(String name, String userId);

    Project removeByName(String name, String userId);

}
