package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    int getSize();

    Task findByName(String name, String userId);

    Task changeStatusById(String id, Status status, String userId);

    Task changeStatusByName(String name, Status status, String userId);

    Task changeStatusByIndex(Integer index, Status status, String userId);

    Task startById(String id, String userId);

    Task startByIndex(Integer index, String userId);

    Task startByName(String name, String userId);

    Task finishById(String id, String userId);

    Task finishByIndex(Integer index, String userId);

    Task finishByName(String name, String userId);

    Task bindTaskToProjectById(String projectId, String taskId, String userId);

    Task unbindTaskById(String id, String userId);

    List<Task> findAllByProjectId(String id, String userId);

    void removeAllTaskByProjectId(String id, String userId);

    Task removeByName(String name, String userId);

}
