package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String id, String userId) throws EmptyIdException;

    Task bindTaskToProject(String projectId, String taskId, String userId) throws AbstractException;

    Task unbindTaskFromProject(String projectId, String taskId, String userId) throws AbstractException;

}
