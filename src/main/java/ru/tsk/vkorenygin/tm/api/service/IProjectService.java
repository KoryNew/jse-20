package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void create(String name, String userId) throws EmptyNameException;

    void create(String name, String description, String userId) throws AbstractException;

    Project findByName(final String name, final String userId) throws EmptyNameException;

    Project changeStatusById(final String id, final Status status, final String userId) throws AbstractException;

    Project changeStatusByName(final String name, final Status status, final String userId) throws AbstractException;

    Project changeStatusByIndex(final Integer index, final Status status, final String userId) throws AbstractException;

    Project startById(String id, String userId) throws AbstractException;

    Project startByIndex(Integer index, String userId) throws AbstractException;

    Project startByName(String name, String userId) throws AbstractException;

    Project finishById(String id, String userId) throws AbstractException;

    Project finishByIndex(Integer index, String userId) throws AbstractException;

    Project finishByName(String name, String userId) throws AbstractException;

    Project updateById(final String id,
                       final String name,
                       final String description,
                       final String userId) throws AbstractException;

    Project updateByIndex(final Integer index,
                          final String name,
                          final String description,
                          final String userId) throws AbstractException;

    Project removeByName(final String name, final String userId) throws EmptyNameException;

}
