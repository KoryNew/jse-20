package ru.tsk.vkorenygin.tm.exception;

public abstract class AbstractException extends RuntimeException{

    public AbstractException() {
        super();
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    public AbstractException(final String message) {
        super(message);
    }

}
