package ru.tsk.vkorenygin.tm.entity;

import ru.tsk.vkorenygin.tm.api.entity.IWBS;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractOwnerEntity implements IWBS {

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private String projectId = null;

    private Date startDate;

    private Date createDate = new Date();

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Name: " + getName() + "; " +
                "Status: " + getStatus() + "; " +
                "Started: " + getStartDate() + "; " +
                "Created: " + getCreateDate() + "; ";
    }

}
