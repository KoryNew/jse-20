package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(currentUserId);
    }

}
