package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task findByName(String name, String userId) {
        if (DataUtil.isEmpty(name)) return null;
        for (Task task : entities) {
            if (task == null) continue;
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task changeStatusById(String id, Status status, String userId) {
        Task task = findById(id, userId);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status, String userId) {
        Task task = findByName(name, userId);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status, String userId) {
        Task task = findByIndex(index, userId);
        if (task == null)
            return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startById(String id, String userId) {
        Task task = findById(id, userId);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(Integer index, String userId) {
        Task task = findByIndex(index, userId);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(String name, String userId) {
        Task task = findByName(name, userId);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(String id, String userId) {
        Task task = findById(id, userId);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index, String userId) {
        Task task = findByIndex(index, userId);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name, String userId) {
        Task task = findByName(name, userId);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId, String userId) {
        Task task = findById(taskId, userId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(String id, String userId) {
        Task task = findById(id, userId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String id, String userId) {
        List<Task> entitiesFound = new ArrayList<>();
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getProjectId())) entitiesFound.add(task);
        }
        if (entitiesFound.size() == 0)
            return null;
        return entitiesFound;
    }

    @Override
    public void removeAllTaskByProjectId(String id, String userId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (!id.equals(task.getProjectId())) entitiesNew.add(task);
        }
        entities = entitiesNew;
    }

    @Override
    public Task removeByName(String name, String userId) {
        Task task = findByName(name, userId);
        if (task == null)
            return null;
        entities.remove(task);
        return task;
    }

}
