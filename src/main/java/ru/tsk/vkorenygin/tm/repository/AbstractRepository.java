package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null) return;
        entities.addAll(collection);
    }

    @Override
    public boolean existsById(String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(Integer index) {
        return index < entities.size();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entitiesSorted = new ArrayList<>(entities);
        entitiesSorted.sort(comparator);
        return entitiesSorted;
    }

    @Override
    public E findById(final String id) {
        if (DataUtil.isEmpty(id)) return null;
        for (final E entity : entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E remove(final E entity) {
        entities.remove(entity);
        return entity;
    }

}
