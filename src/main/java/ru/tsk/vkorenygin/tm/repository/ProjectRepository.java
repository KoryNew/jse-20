package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(String name, String userId) {
        if (DataUtil.isEmpty(name)) return null;
        for (Project project : entities) {
            if (project == null) continue;
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project changeStatusById(String id, Status status, String userId) {
        Project project = findById(id, userId);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status, String userId) {
        Project project = findByName(name, userId);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status, String userId) {
        Project project = findByIndex(index, userId);
        if (project == null)
            return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startById(String id, String userId) {
        Project project = findById(id, userId);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(Integer index, String userId) {
        Project project = findByIndex(index, userId);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name, String userId) {
        Project project = findByName(name, userId);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id, String userId) {
        Project project = findById(id, userId);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(Integer index, String userId) {
        Project project = findByIndex(index, userId);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String name, String userId) {
        Project project = findByName(name, userId);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project removeByName(String name, String userId) {
        Project project = findByName(name, userId);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

}
