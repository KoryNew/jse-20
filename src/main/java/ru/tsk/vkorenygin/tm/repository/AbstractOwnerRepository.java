package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.EntityNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {



    public boolean existsById(String id, String userId) {
        E entity = findById(id);
        return entity != null;
    }

    public boolean existsByIndex(Integer index, String userId) {
        return index < entities.size();
    }

    @Override
    public void remove(E entity, String userId) {
        List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(String userId) {
        List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @Override
    public List<E> findAll(String userId) {
        List<E> ownerEntities = new ArrayList<>();
        for (E entity : entities) {
            if (userId.equals(entity.getUserId())) ownerEntities.add(entity);
        }
        return ownerEntities;
    }

    public List<E> findAll(Comparator<E> comparator, String userId) {
        List<E> ownerEntities = findAll(userId);
        ownerEntities.sort(comparator);
        return ownerEntities;
    }

    @Override
    public E findById(String id, String userId) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        for (E entity : ownerEntities) {
            if (id.equals(entity.getId())) return entity;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public E findByIndex(int index, String userId) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IncorrectIndexException();
        return ownerEntities.get(index);
    }

    @Override
    public E removeById(String id, String userId) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        E entity = findById(id, userId);
        ownerEntities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(int index, String userId) throws AbstractException {
        List<E> ownerEntities = findAll(userId);
        E entity = findByIndex(index, userId);
        ownerEntities.remove(entity);
        return entity;
    }

}
